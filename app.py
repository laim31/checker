from flask import Flask, render_template, request
import requests
import random
import re

app = Flask(__name__)

def generate_random_digits(length=4):
    return ''.join(random.choices('0123456789', k=length))

def is_valid_domain_name(domain_name):
    valid_chars_pattern = r'^[a-zA-Z0-9-]+$'
    return bool(re.match(valid_chars_pattern, domain_name))

def check_domain_availability_godaddy(domain_name):
    api_key = 'gHzd4aGHv8bX_UJgqGfvtSa3qvB9u8qGycN'
    api_secret = 'GuDasNf1cyXH2dJF9D4NRy'
    api_url = f'https://api.godaddy.com/v1/domains/available?domain={domain_name}'

    headers = {
        'Authorization': f'sso-key {api_key}:{api_secret}'
    }

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        availability_result = response.json()
        return availability_result['available']
    else:
        print(f"Ошибка при проверке доступности домена: {response.status_code}")
        return False

@app.route('/', methods=['GET', 'POST'])
def index():
    error_messages = []

    if request.method == 'POST':
        user_input = request.form['prefix'].lower()
        num_domains = int(request.form.get('num_domains', 3))
        num_digits = int(request.form.get('num_digits', 4))
        digits_position = request.form.get('digits_position')
        generate_without_hyphen = 'v3' in request.form

        generated_domains = []

        if is_valid_domain_name(user_input):
            while num_domains > 0:
                random_digits = generate_random_digits(num_digits)

                if generate_without_hyphen:
                    if digits_position == 'before':
                        domain_to_check = f'{random_digits}{user_input}.com'
                    else:
                        domain_to_check = f'{user_input}{random_digits}.com'
                else:
                    if digits_position == 'before':
                        domain_to_check = f'{random_digits}-{user_input}.com'
                    else:
                        domain_to_check = f'{user_input}-{random_digits}.com'

                if check_domain_availability_godaddy(domain_to_check):
                    generated_domains.append(domain_to_check)
                    num_domains -= 1
                else:
                    error_messages.append(f"{domain_to_check} уже занят.")
                    break
        else:
            error_messages.append(f"{user_input} содержит недопустимые символы.")

        return render_template('index.html', generated_domains=generated_domains, error_messages=error_messages)

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
